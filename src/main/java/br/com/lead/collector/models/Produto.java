package br.com.lead.collector.models;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Produto")
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;
    @NotNull(message = "campo nome não pode ser null")
    @NotBlank(message = "Campo nome em branco")
    @Size(min = 3, message = "nome minimo 3 posicoes")
    private  String nome;
    @NotNull(message = "campo nome não pode ser null")
    @NotBlank(message = "Campo nome em branco")
    @Size(min = 10, message = "nome minimo 3 posicoes")
    private  String descricao;
    @NotNull(message = "campo valor não pode ser null")
    private double preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
