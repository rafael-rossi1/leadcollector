package br.com.lead.collector.controller;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadServices;
import br.com.lead.collector.services.ProdutoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoServices produtoServices;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto gravar(@RequestBody @Valid Produto produto) {
        Produto objetoProduto = produtoServices.salvarProduto(produto);
        return objetoProduto;

    }

    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoServices.lerTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto pesquisarPorID(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoServices.buscarProdutoPeloID(id);
            return produto;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable int id,@RequestBody @Valid Produto produto) {
        try{
            Produto produtoDB = produtoServices.atualizarProduto(id, produto);
            return produtoDB;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto( @PathVariable(name = "id") int id){
        try{
            produtoServices.deletarProduto(id);
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }

    }
}
