package br.com.lead.collector.controller;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.services.LeadServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadServices leadServices;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead gravar(@RequestBody @Valid Lead lead) {
        Lead objetoLead = leadServices.salvarLead(lead);
        return objetoLead;

    }

    @GetMapping
    public Iterable<Lead> lerTodosOsLeads(){
        return leadServices.lerTodosOsLeads();
    }

    @GetMapping("/{id}")
     public Lead pesquisarPorID(@PathVariable(name = "id") int id){
        try{
            Lead lead = leadServices.buscarLeadPeloID(id);
            return lead;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable int id,@RequestBody @Valid Lead lead) {
        try{
            Lead leadDB = leadServices.atualizarLead(id, lead);
            return leadDB;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead( @PathVariable(name = "id") int id){
        try{
            leadServices.deletarLead(id);
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }

    }
}





