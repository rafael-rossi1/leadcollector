package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoServices {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto objetoProduto = produtoRepository.save(produto);
        return objetoProduto;
    }

    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoRepository.findAll();
    }

    public Produto buscarProdutoPeloID(int id) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if(produtoOptional.isPresent()){
            Produto produto = produtoOptional.get();
            return produto;
        }else {
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public Produto atualizarProduto(int id,Produto produto){
        Produto produtoDB = buscarProdutoPeloID(id);
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        }else{
            throw new RuntimeException("Produto não encontrado");
        }

    }
}
